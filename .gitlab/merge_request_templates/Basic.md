<!-- Thanks for your PR, your contribution is appreciated! -->

- [ ] I have followed the [MR section of the contributing guide](https://gitlab.com/sia-insa-lyon/dev/portailva-2/portailva-website/-/blob/master/CONTRIBUTING.md#definition-of-done).

<!-- Add description if necessary or if your modification follow partially what is described in the issue solved -->

/assign me
/assign @firemat
/label "Merge request::Needs review"

Closes #`issue_id`

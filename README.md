[![Licence GPL](http://img.shields.io/badge/license-GPL-green.svg)](http://www.gnu.org/licenses/quick-guide-gplv3.fr.html)

# Portail VA 2

Website to display INSA association and to provide information about them.
To run this app, you will need to have `npm` on your computer.

## Installation
Clone this repository, copy the `.env.prod` as `.env` and run `npm install --save --save-dev && npm run start`.

## Available NPM scripts

### `npm run start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run linter`

Check the conformity of the code against the style guideline of the SIA.

## Built With
- [Create-React-App](https://facebook.github.io/create-react-app) &mdash; Our bootstrap utility.
- [ReactJS](https://reactjs.org/) &mdash; Our main front framework.
- [MaterialUI](https://material-ui.com/) &mdash; Our style framework.

And a lot of other dependencies you can find in package.json file.


## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
Portail VA 2 - Website to display INSA association and to provide information about them
Copyright (C) 2019 SIA INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

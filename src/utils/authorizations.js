/**
 * Collection of authorization functions, to verify access to a given resource
 */
import {useKeycloak} from "@react-keycloak/web";
import axios from "axios";

/**
 * Custom hook to make use of Keycloak authentication to make requests
 * @returns {{privateRequest: AxiosInstance, keycloak: authClient, initialized: boolean}}
 */
export function useKeycloakAuth() {
    const kc = useKeycloak();
    const { keycloak, initialized } = kc;

    const tokenHeader = initialized ? {Authorization: "Bearer " + keycloak.token} : {};
    const privateRequest = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: tokenHeader,
    });

    return {
        initialized,
        keycloak,
        privateRequest
    };
}

/**
 * Matches roles against a list of authorized roles
 * Returns true if any of the user roles matches the resourceAccess list
 * @param roles : string[] list of roles, as a string array
 * @param resourceAccess : keycloak token resourceAccess object
 * @returns {boolean} :  if the user can access the resource or not
 */
export default function isAuthorized(roles, resourceAccess) {
    return roles.some(r => {
        return resourceAccess["portailva2-front"].roles.indexOf(r) >= 0;
    });
}

/**
 * Given a route (as defined in route.js),
 * returns true if the authenticated user can access the given route.
 * If the route doesn't have a role list, the route is always accessible
 * @param route : object containing (or not) a list of roles under the property "roles"
 * @param resourceAccess : keycloak token resourceAccess object
 * @returns {boolean}
 */
export const canUseRoute = (route, resourceAccess) => {
    return !route.roles || (resourceAccess && isAuthorized(route.roles, resourceAccess));
};

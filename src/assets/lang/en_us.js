export const local = {
    Component: {
        Footer: {
            "seeSection": "See on"
        },
        SnackBarComponent: {
            "undoButton": "Undo"
        },
        AppDrawer: {
            translateButton: "Passer au français",
            loginButton: "Log in"
        }
    },
    Request: {
        error: {
            BadRequest: "An error happened, please refresh the page!",
            ErrorLogin: "The authentication token has expired, please contact a SIA member!",
            ErrorForbidden: "You are trying to access a forbidden resource!",
            ErrorNotFound: "This resource cannot be found!",
            Default: "An error happened, please contact a SIA member!",
        },
        sso: {
            waiting: "Loading authentication module...",
        }
    },
    Route: {
        "/": "Home",
        "/login": "Log in",
        "/logout": "Log out",
        "/profile": "Profile",
        "/admin": "Administration",
    },
    View: {
        AdminPanel: {
            "title": "Administrator View"
        }
    }
};

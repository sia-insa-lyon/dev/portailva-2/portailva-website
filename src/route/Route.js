import React from "react";
import {Home as HomeIcon, Person as PersonIcon, SupervisorAccount as SupervisorAccountIcon} from '@material-ui/icons';

import HomePage from "../view/HomePage";
import Profile from "../view/Profile";
import AdminPanel from "../view/AdminPanel";

export const route = [[
    {
        render: HomePage,
        path: "/",
        icon: <HomeIcon/>
    },
]];

export const private_route = [[
    {
        render: Profile,
        path: "/profile",
        icon: <SupervisorAccountIcon/>,
        roles: ["test_role"]
    },
    {
        render: AdminPanel,
        path: "/admin",
        icon: <PersonIcon/>,
        roles: ["administrator"]
    }
]];


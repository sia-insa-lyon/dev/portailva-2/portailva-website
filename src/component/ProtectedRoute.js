import { useKeycloak } from '@react-keycloak/web';
import React, {useContext} from 'react';
import { Route } from 'react-router-dom';
import * as PropTypes from "prop-types";

import isAuthorized from "../utils/authorizations";
import {Typography} from "@material-ui/core";
import LanguageContext from "../LanguageContext";


ProtectedRoute.propTypes = {
    roles: PropTypes.array.isRequired,
    render: PropTypes.func.isRequired,
};

/**
 * Protected route component
 * Redirects to auth page if the user is not authentified,
 * and shows an error if the user can't access the resource
 */
export function ProtectedRoute({roles, render, ...props }) {
    const local = useContext(LanguageContext);
    const { keycloak, initialized } = useKeycloak();

    return (
        initialized ?
            <Route
                {...props}
                render={(props) => {
                    return keycloak.authenticated
                        ? (isAuthorized(roles, keycloak.resourceAccess) ? render(props) : <Typography>{local.Request.error.ErrorForbidden}</Typography>)
                        : keycloak.login();
                }}
            /> : <Typography>{local.Request.sso.waiting}</Typography>
    );
}

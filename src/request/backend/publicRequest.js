import axios from 'axios';
import { rematchError } from '../requestErrors';

const publicRequest = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
});

export const getListAsso = () => {
    return publicRequest.get("directory/")
        .then(response => {
            return response.data;
        }).catch(rematchError);
};

export const getDetailAsso = (id) => {
    return publicRequest.get("directory/" + id + "/")
        .then(response => {
            return response.data;
        }).catch(rematchError);
};